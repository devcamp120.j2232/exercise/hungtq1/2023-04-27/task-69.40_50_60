package com.devcamp.entityrelationshipcustomerorderpaymentproduct.model;

import java.util.*;

import javax.persistence.*;

import org.springframework.data.annotation.CreatedDate;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "orders")
public class Order {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "create_at", nullable = true, updatable = false)
  @CreatedDate
  @JsonFormat(pattern = "yyyy-MM-dd")
  private Date createdAt;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer", nullable = false)
  private Customer customer;

  @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "order")
  private Payment payment;

  @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "orders")
  private Set<Product> products;

  public Order() {
  }

  public Order(Long id, Date createdAt, Customer customer, Payment payment, Set<Product> products) {
    this.id = id;
    this.createdAt = createdAt;
    this.customer = customer;
    this.payment = payment;
    this.products = products;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public Payment getPayment() {
    return payment;
  }

  public void setPayment(Payment payment) {
    this.payment = payment;
  }

}
